package com.spring.serviceSpring.userController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.serviceSpring.dominio.Producto;
import com.spring.serviceSpring.repository.Repo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ControllerProductos {
    Logger log = LoggerFactory.getLogger(ControllerProductos.class);
    @Autowired
    private Repo repo;

    @PutMapping("/producto")
    public void create(@RequestBody Producto producto) {
        repo.save(producto);
    }


    @GetMapping("/producto/{id}")
    @ResponseBody
    public String productoId(@PathVariable int id) throws UserPrincipalNotFoundException {
        try {
            Optional<Producto> user = repo.findById(id);
            if(user.isPresent()){
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(user.get());
                log.info("Usuario encontrado con exito!");
                return json;
            }else{
                log.info("Usuario no encontrado");
            }
        }catch (Exception e){
            log.info("Error buscando en cliente: " + e );
        }
        return "";
    }

    @GetMapping("/listaProductos")
    public List<Producto> showAllUsers() throws SQLException, ClassNotFoundException {
        return (List<Producto>) repo.findAll();
    }

    @GetMapping("/productoNombre/{name}")
    @ResponseBody
    public List<Producto> users(@PathVariable String name) throws UserPrincipalNotFoundException {
        List<Producto> lista = (List<Producto>) repo.findAll();
        List<Producto>  ret= new ArrayList<>();
        for(Producto producto : lista){
            if(producto.getName().equals(name))
                ret.add(producto);
        }
        return ret;
    }

    @PostMapping("/updateStock/{id}/{cant}")
    public void updateStock(@PathVariable int id, @PathVariable int cant) {
        Producto producto = repo.findById(id).get();
        int total= cant + producto.getStock();
        producto.setStock(total);
        repo.save(producto);
    }


}
