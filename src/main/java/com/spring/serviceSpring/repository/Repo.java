package com.spring.serviceSpring.repository;

import com.spring.serviceSpring.dominio.Producto;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface Repo extends CrudRepository<Producto, Integer>{
}